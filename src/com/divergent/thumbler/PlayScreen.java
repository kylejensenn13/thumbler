package com.divergent.thumbler;

import java.io.OutputStreamWriter;
import java.util.Random;

import com.divergent.thumbler.SharedPrefManager;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Toast;

public class PlayScreen extends View {

	// This is the view's bounds
    private int xViewMin = 0; 
    private int xViewMax;
    private int yViewMin = 0;
    private int yViewMax;
    
    // Player
    private float playerRadius = 20, playerX = playerRadius + 10, playerY = playerRadius + 20;     // Player's radius, Player's center (x,y)
    
    
    private RectF playerBounds; // Player
    private RectF continuePlaying; // Pause Screen continue button.
    private RectF rowBlock; // The first block generated.
    private RectF rowBlock2; // The block after the gap.
    public RectF startPlaying; // Start Playing button.
    public RectF rowGlow; // Glow for rowBlock.
    public RectF rowGlow2; // Glow for rowBlock2.
    public RectF pausedBounds; // Bounds for Paused title.
    public RectF meteorBounds;
    public RectF pointLossBounds;
    public RectF pointGainBounds;
    public RectF lifePackBounds;

    // Colors
    private Paint colorGreen, colorBlack, colorWhite, colorCyan,
    colorYellow, colorRed, colorBlue, colorMagenta, colorOrange, glowGreen, glowWhite, glowCyan,
    glowYellow, glowRed, glowBlue, glowMagenta, glowOrange, scoreColor, transparent, startPaint; 

    private Double scrollSpeed = (double) 0; // Initial scroll speed.
    private double oldSpeed = -4.5; // Speed once game is started. Gets reset on pause event.
    public double blockY[] = new double[50]; // Array of Y positions for blocks.
    public double blockX1[] = new double[50]; // Array of the first blocks.
    public double blockX2[] = new double[50]; // Array of the second blocks.
  
    private int rowSpace = 250; // Space between rows.
    public int screenHeight, screenWidth; // Screen height and width.
    public static Integer finalScore = 0; // Players final score.
    public int blockcolor[] = new int[50]; // Array for the colors the block can choose.
    
    
    private Integer score = 0; // Counter for the score.
    public float halfgap; // Midpoint of gap. Used for positioning the start button.
    
    private String scoreText = ""; // String display of your score.
    public int HighScore;
    
    public Random random; // My random number generator.

    public boolean Playing; // Triggered on when player starts.
    public boolean Paused = false; // Triggered on only after game has started and player's finger is off screen.
    public boolean meteorOn = false, pointLossOn = false, pointGainOn = false, lifePackOn = false, bulletOn = false;


    public Bitmap pauseBackground; // Pause background.
    public Bitmap pausedText; // Paused title.
    public Bitmap cont; // Continue Playing.
    public Typeface scoreFont; // Font for score.
    
    
    public AssetManager Assets = getContext().getAssets();
    public Typeface font;
    
    public long randomEvent;
    public int eventRandomX;
    
    public Integer event;
    public Integer eventY;
    
    public static Integer LIVES = 0;
    
    public boolean test = true;
    
    
    // Constructor
    public PlayScreen(Context context) {
        super(context);
        
        /**
         * Instantiate all variables.
         */
        playerBounds = new RectF();
        rowBlock = new RectF();
        rowBlock2 = new RectF();
        startPlaying = new RectF();
        continuePlaying = new RectF();
        rowGlow = new RectF();
        rowGlow2 = new RectF();
        pausedBounds = new RectF();
        meteorBounds = new RectF();
        pointLossBounds = new RectF();
        pointGainBounds = new RectF();
        lifePackBounds = new RectF();
        
        
        ///// Colors /////
        
        colorGreen = new Paint();
        colorBlack = new Paint();
        colorWhite = new Paint();
        colorBlue = new Paint();
        colorYellow = new Paint();
        colorRed = new Paint();
        colorCyan = new Paint();
        colorMagenta = new Paint();
        colorOrange = new Paint();
        scoreColor = new Paint();
        
        glowGreen = new Paint();
        glowWhite = new Paint();
        glowBlue = new Paint();
        glowYellow = new Paint();
        glowRed = new Paint();
        glowCyan = new Paint();
        glowMagenta = new Paint();
        glowOrange = new Paint();

        transparent = new Paint();
        startPaint = new Paint();
        
		// Set my basic color's
        colorGreen.setColor(Color.rgb(17, 255, 0));        
        colorBlack.setColor(Color.rgb(0, 0, 0));
        colorWhite.setColor(Color.rgb(255, 255, 255));
        colorBlue.setColor(Color.rgb(0, 0, 255));
        colorYellow.setColor(Color.rgb(243, 243, 21));
        colorRed.setColor(Color.rgb(255, 0, 0));
        colorCyan.setColor(Color.rgb(51, 255, 255));
        colorMagenta.setColor(Color.rgb(255, 0, 255));
        colorOrange.setColor(Color.rgb(255, 119, 0));
        transparent.setColor(Color.TRANSPARENT);
        startPaint.setColor(Color.rgb(255,  119, 0));
        
        // Set glow colors
        glowGreen.setColor(Color.argb(200, 17, 255, 0)); 
        glowWhite.setColor(Color.argb(200, 255, 255, 255));
        glowBlue.setColor(Color.argb(200, 0, 0, 255));
        glowYellow.setColor(Color.argb(200, 243, 243, 21));
        glowRed.setColor(Color.argb(200, 255, 0, 0));
        glowCyan.setColor(Color.argb(200, 51, 255, 255));
        glowMagenta.setColor(Color.argb(200, 255, 0, 255));
        glowOrange.setColor(Color.argb(200, 255, 119, 0));
        
        // Score text info
        scoreColor.setColor(Color.rgb(255, 255, 255));
        scoreColor.setTextSize(60f);
        font = Typeface.createFromAsset(Assets, "fonts/EuphoriaScript.otf");
  	    scoreColor.setTypeface(font);
        
  	    ///// End Colors /////
  	    
  	    
        
        random = new Random();
        Playing = false;
        event = random.nextInt(4);
        
        pauseBackground = BitmapFactory.decodeResource(getResources(), R.drawable.pausebackground);
        pausedText = BitmapFactory.decodeResource(getResources(), R.drawable.paused);
        cont = BitmapFactory.decodeResource(getResources(), R.drawable.cont);
        

  	    
        // Get window size and save it to screenWidth and screenHeight.
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size); 
        screenWidth = size.x;
        screenHeight = size.y;
  
        int nor = screenHeight / rowSpace; // Number of rows to be displayed.
        
        /**
         * Generate rows and gaps at set intervals.
         * Chooses a random color for each row.
         */
        for (int row = 0; row < nor; row ++) { 
        	final int gap = random.nextInt(150) + 100;
        	final int block = random.nextInt(screenWidth - gap);
        
        	blockY[row] = screenHeight * 1/2 + row * rowSpace; // Positions first block at center screen.
        	blockX1[row] = block;
        	blockX2[row] = block + gap; 
        	blockcolor[row] = random.nextInt(8);
        	
        	halfgap = (float) (blockX2[0] + blockX1[0]) / 2; // Used for centering start button to first gap.
        	
        				
        }
        
        randomEvent = System.currentTimeMillis() + random.nextInt(15000) + 5000;
        eventRandomX = random.nextInt(screenWidth);
        
        meteorBounds.set(eventRandomX, 0, eventRandomX + 50, 50);
        pointLossBounds.set(eventRandomX, 0, eventRandomX + 50, 50);
        pointGainBounds.set(eventRandomX, 0, eventRandomX + 50, 50);
        lifePackBounds.set(eventRandomX, 0, eventRandomX + 50, 50);
       
        scoreText = score.toString();
    }

    // Called back to draw the view. Also called by invalidate().
	@Override protected void onDraw(Canvas canvas) {
		
		// Once game is started and paused is set to true, pause all counters.			
		if (Playing == true && Paused == false) { 
			scrollSpeed = oldSpeed;
			scrollSpeed -= 0.001;
	        oldSpeed = scrollSpeed;
		
		} else if (Paused == true) {
			scrollSpeed = (double) 0;
			scrollSpeed -= 0;
		}
		

	
		int rowBlockHeight = screenHeight / 32;
		
    	playerBounds.set(playerX - playerRadius,(float) playerY - playerRadius, playerX + playerRadius, (float) playerY + playerRadius);
    	
        int nor = screenHeight / rowSpace;
        
        for (int row = 0; row < nor; row ++) {	        	
        	blockY[row] = blockY[row] + scrollSpeed;
        	blockX1[row] = blockX1[row];
        	blockX2[row] = blockX2[row];
        	
	    	rowBlock.set(0, (float) blockY[row],(float) blockX1[row], (float) blockY[row] + rowBlockHeight);
	    	rowBlock2.set((float) blockX2[row], (float)blockY[row], (float) screenWidth, (float) blockY[row] + rowBlockHeight);

	    	rowGlow.set(-20, (float) blockY[row] - 10, (float) blockX1[row] + 10, (float) blockY[row] + rowBlockHeight + 10);
	    	rowGlow2.set((float) blockX2[row] - 10, (float) blockY[row] - 10, (float) screenWidth + 20, (float) blockY[row] + rowBlockHeight + 10);
    
	    	if (blockY[row] < screenHeight) {
			    if (blockcolor[row] == 0) {
			    	canvas.drawRoundRect(rowGlow, 10, 10, glowGreen);
			    	canvas.drawRoundRect(rowGlow2, 10, 10, glowGreen);
		    		canvas.drawRoundRect(rowBlock, 5, 5, colorBlack);
		    		canvas.drawRoundRect(rowBlock2, 5, 5, colorBlack);	
		    	}
		    	if (blockcolor[row] == 1) {
			    	canvas.drawRoundRect(rowGlow, 10, 10, glowRed);
			    	canvas.drawRoundRect(rowGlow2, 10, 10, glowRed);
		    		canvas.drawRoundRect(rowBlock, 5, 5, colorBlack);
		    		canvas.drawRoundRect(rowBlock2, 5, 5, colorBlack);	
		    	}
			    if (blockcolor[row] == 2) {
			    	canvas.drawRoundRect(rowGlow, 10, 10, glowBlue);
			    	canvas.drawRoundRect(rowGlow2, 10, 10, glowBlue);
		    		canvas.drawRoundRect(rowBlock, 5, 5, colorBlack);
		    		canvas.drawRoundRect(rowBlock2, 5, 5, colorBlack);	    	   
			    }
			    if (blockcolor[row] == 3) {
			    	canvas.drawRoundRect(rowGlow, 10, 10, glowWhite);
			    	canvas.drawRoundRect(rowGlow2, 10, 10, glowWhite);
		    		canvas.drawRoundRect(rowBlock, 5, 5, colorBlack);
		    		canvas.drawRoundRect(rowBlock2, 5, 5, colorBlack);		    		
			    }
			    if (blockcolor[row] == 4) {
			    	canvas.drawRoundRect(rowGlow, 10, 10, glowYellow);
			    	canvas.drawRoundRect(rowGlow2, 10, 10, glowYellow);
		    		canvas.drawRoundRect(rowBlock, 5, 5, colorBlack);
		    		canvas.drawRoundRect(rowBlock2, 5, 5, colorBlack);		    		 
			    }	  
			    if (blockcolor[row] == 5) {
			    	canvas.drawRoundRect(rowGlow, 10, 10, glowCyan);
			    	canvas.drawRoundRect(rowGlow2, 10, 10, glowCyan);
		    		canvas.drawRoundRect(rowBlock, 5, 5, colorBlack);
		    		canvas.drawRoundRect(rowBlock2, 5, 5, colorBlack);		    		
			    }	
			    if (blockcolor[row] == 6) {
			    	canvas.drawRoundRect(rowGlow, 10, 10, glowMagenta);
			    	canvas.drawRoundRect(rowGlow2, 10, 10, glowMagenta);
		    		canvas.drawRoundRect(rowBlock, 5, 5, colorBlack);
		    		canvas.drawRoundRect(rowBlock2, 5, 5, colorBlack);		    		
			    }
			    if (blockcolor[row] == 7) {
			    	canvas.drawRoundRect(rowGlow, 10, 10, glowOrange);
			    	canvas.drawRoundRect(rowGlow2, 10, 10, glowOrange);
		    		canvas.drawRoundRect(rowBlock, 5, 5, colorBlack);
		    		canvas.drawRoundRect(rowBlock2, 5, 5, colorBlack);
		    		
			    }
	    	}
	    	
	    	if (blockY[row] < 0) {

	        	final int gap = random.nextInt(150) + 100;
	        	final int block = random.nextInt(screenWidth - gap);
	        	
	        	if (row == 0) {
	        		blockY[row] = blockY[nor - 1] + rowSpace + rowBlockHeight;
	        	} else {
	        		blockY[row] = blockY[row - 1] + rowSpace;
	        	}
	        	
	    		blockX1[row] = block;
	    		blockX2[row] = block + gap;
	    		blockcolor[row] = random.nextInt(8);
	    		score += 1;
	            
	            scoreText = score.toString();
	    		
	    	}
	        	    	
        	if (playerBounds.bottom > rowBlock.top && playerBounds.top < rowBlock.bottom && (playerBounds.left < blockX1[row] || playerBounds.right > blockX2[row])) {
        		
        		youLose();

        	}
        }

        
        if (score < 0) {
        	youLose();
        }
        

        float startRad = 30f;

    	startPlaying.set(halfgap - startRad, (float) (blockY[0] - rowSpace) + startRad * 2, halfgap + startRad, (float) blockY[0] - rowSpace); // Positions the start button between the first gap.

    	playerBounds.set(playerX - playerRadius,(float) playerY - playerRadius, playerX + playerRadius, (float) playerY + playerRadius);
        
    	pausedBounds.set(screenWidth / 2 - pausedText.getWidth() / 2, 100, pausedBounds.left + 436, pausedBounds.top + 170);
   		canvas.drawOval(playerBounds, transparent); // Draw player
        canvas.drawText(scoreText, 10, 40, scoreColor);
        
        if (Playing == false) canvas.drawOval(startPlaying, startPaint);	
        
        if (Paused == true) {
        	
        	continuePlaying.set(playerBounds.centerX() - cont.getWidth() / 2, playerBounds.centerY() - cont.getHeight() / 2, playerBounds.left + cont.getWidth() / 2, playerBounds.top + cont.getHeight() / 2);
        	canvas.drawBitmap(pauseBackground, 0, 0, null);
        	canvas.drawBitmap(pausedText, null, pausedBounds, null);
        	canvas.drawBitmap(cont, null, continuePlaying, colorRed);
        }
	      
        
        if (score > 10 && System.currentTimeMillis() > randomEvent) {
        		

			if (meteorOn == false && pointLossOn == false && pointGainOn == false && lifePackOn == false && bulletOn == false) {
        		
        		event = random.nextInt(4);
        		eventY = random.nextInt(15) + 5;
        		
        		System.out.println("The event number is!" + event.toString() + "y = " + eventY.toString());
        		
        			if (event == 0) {
        				meteorOn = true;
        			} else if (event == 1) {
        				pointLossOn = true;
        			} else if (event == 2) {
        				pointGainOn = true;
        			} else if (event == 3) {
        				lifePackOn = true;
        			}
        		
        	}

        	// Events // 
        	
        	if (meteorOn == true) {
        		MeteorEvent(5, eventY);
        		canvas.drawOval(meteorBounds, colorRed);	
        	}
        	if (pointLossOn == true) {
        		PointLossEvent(5, eventY);
        		canvas.drawOval(pointLossBounds, colorBlue);
        	}
        	if (pointGainOn == true) {
        		PointGainEvent(5, eventY);
        		canvas.drawOval(pointGainBounds, colorGreen);
        	}
        	if (lifePackOn == true) {
        		LifePackEvent(5, eventY);
        		canvas.drawOval(lifePackBounds, colorWhite);
        	}
        	
        }
        
        
        if (LIVES > 0) {
        	canvas.drawText(LIVES.toString(), getWidth() - 50, 50, scoreColor);
        }

		canvas.drawText(scrollSpeed.toString(), 200, 300, scoreColor);
        // Delay	
        try { Thread.sleep(10); }
        catch (InterruptedException e){}
        invalidate(); // Force a re-draw
    }

    // Called back when the view is first created or its size changes.
    @Override public void onSizeChanged(int w, int h, int oldW, int oldH) {
    
        // Set the movement bounds for the ball
        xViewMax = w - 1;
        yViewMax = h - 1;

    }
    

	public void highscoreSaver() {
	
		if(finalScore > SharedPrefManager.getHighScore()) {
            SharedPrefManager.SetHighScore(finalScore);        		
    		SharedPrefManager.StoreToPref();
    		}
		
	}
	public void youLose() {

    		View LoseScreen = new LoseScreen(getContext());
            ViewGroup parent = (ViewGroup) getParent();
    		finalScore = score;
    		
            highscoreSaver();
            
    		parent.addView(LoseScreen);
    		parent.removeView(this);
    		parent.setBackgroundResource(R.drawable.background);
	
	}

	public void MeteorEvent(float slopeX, float slopeY) {
		
    	if (eventRandomX < screenWidth / 2) {
    	meteorBounds.left += slopeX; meteorBounds.right += slopeX;
    	} else {
    	meteorBounds.left -= slopeX; meteorBounds.right -= slopeX;
    	}
    	meteorBounds.top += slopeY; meteorBounds.bottom += slopeY;
    	
    	if(meteorBounds.centerX() > screenWidth || meteorBounds.centerY() > screenHeight) {
    		
    		randomEvent = System.currentTimeMillis() + random.nextInt(5000) + 3000;
    		eventRandomX = random.nextInt(screenWidth - 20) + 20;
            meteorBounds.set(eventRandomX, 0, eventRandomX + 50, 50);  
            meteorOn = false;
    	}	
        if (playerBounds.bottom > meteorBounds.top && playerBounds.top < meteorBounds.bottom && playerBounds.left > meteorBounds.left && playerBounds.right < meteorBounds.right)
    		youLose();
	}
	
	public void PointLossEvent(float slopeX, float slopeY) {
		
    	if (eventRandomX < screenWidth / 2) {
    		pointLossBounds.left += slopeX; pointLossBounds.right += slopeX;
    	} else { 
    		pointLossBounds.left -= slopeX; pointLossBounds.right -= slopeX;
    	}
    	
    	pointLossBounds.top += slopeY; pointLossBounds.bottom += slopeY;
    	
    	if((pointLossBounds.centerX() > screenWidth) || (pointLossBounds.centerY() > screenHeight)) {
    		pointLossOn = false;
    		randomEvent = System.currentTimeMillis() + random.nextInt(5000) + 3000;
    		eventRandomX = random.nextInt(screenWidth - 20) + 20;
    		pointLossBounds.set(eventRandomX, 0, eventRandomX + 50, 50);


    	} else if (playerBounds.bottom > pointLossBounds.top && playerBounds.top < pointLossBounds.bottom && playerBounds.left > pointLossBounds.left && playerBounds.right < pointLossBounds.right) {
            score -= 10;
            scoreText = score.toString();
    		pointLossOn = false;
    		randomEvent = System.currentTimeMillis() + random.nextInt(5000) + 3000;
    		eventRandomX = random.nextInt(screenWidth - 20) + 20;
    		pointLossBounds.set(eventRandomX, 0, eventRandomX + 50, 50);

    	}	
	}
	
	public void PointGainEvent(float slopeX, float slopeY) {
		
    	if (eventRandomX < screenWidth / 2) {
    	pointGainBounds.left += slopeX; pointGainBounds.right += slopeX;
    	} else {
    	pointGainBounds.left -= slopeX; pointGainBounds.right -= slopeX;
    	}
    	pointGainBounds.top += slopeY; pointGainBounds.bottom += slopeY;
    	
    	if(pointGainBounds.centerX() > screenWidth || pointGainBounds.centerY() > screenHeight) {
            pointGainOn = false;
    		randomEvent = System.currentTimeMillis() + random.nextInt(5000) + 3000;
    		eventRandomX = random.nextInt(screenWidth - 20) + 20;
    		pointGainBounds.set(eventRandomX, 0, eventRandomX + 50, 50);  

    	} else if (playerBounds.bottom > pointGainBounds.top && playerBounds.top < pointGainBounds.bottom && playerBounds.left > pointGainBounds.left && playerBounds.right < pointGainBounds.right) {
            score += 10;
            scoreText = score.toString();
    		pointGainOn = false;
    		randomEvent = System.currentTimeMillis() + random.nextInt(5000) + 3000;
    		eventRandomX = random.nextInt(screenWidth);
    		pointGainBounds.set(eventRandomX, 0, eventRandomX + 50, 50);
	
    	}
    }
	
	public void LifePackEvent(float slopeX, float slopeY) {
		
    	if (eventRandomX < screenWidth / 2) {
    	lifePackBounds.left += slopeX; lifePackBounds.right += slopeX;
    	} else {
    	lifePackBounds.left -= slopeX; lifePackBounds.right -= slopeX;
    	}
    	lifePackBounds.top += slopeY; lifePackBounds.bottom += slopeY;
    	
    	if(lifePackBounds.centerX() > screenWidth || lifePackBounds.centerY() > screenHeight) {
            lifePackOn = false;
    		randomEvent = System.currentTimeMillis() + random.nextInt(5000) + 3000;
    		eventRandomX = random.nextInt(screenWidth - 20) + 20;
    		lifePackBounds.set(eventRandomX, 0, eventRandomX + 50, 50);  

    	} else if (playerBounds.bottom > lifePackBounds.top && playerBounds.top < lifePackBounds.bottom && playerBounds.left > lifePackBounds.left && playerBounds.right < lifePackBounds.right) {

    		LIVES += 1;
    		
    		lifePackOn = false;
    		randomEvent = System.currentTimeMillis() + random.nextInt(5000) + 3000;
    		eventRandomX = random.nextInt(screenWidth);
    		lifePackBounds.set(eventRandomX, 0, eventRandomX + 50, 50);
	
    	}
    }
	
	public void BulletEvent() {
		
		long test = System.currentTimeMillis();
		
		Double oldSpeed = scrollSpeed;
		scrollSpeed += scrollSpeed * 0.25;
		


		if (System.currentTimeMillis() > test + 5000) {
			
			scrollSpeed = oldSpeed;
			
		}
		
	}
	

		
		

    
    public boolean onTouchEvent(MotionEvent e) {

    	// Get finger's X and Y position
        float x = e.getX();
        float y = e.getY();

        if(x > startPlaying.left || x < startPlaying.right && y < startPlaying.bottom && y > startPlaying.top) {
        	
        	Playing = true;     	
        }
        
        if(x > continuePlaying.left && x < continuePlaying.right && y < continuePlaying.bottom && y > continuePlaying.top) {
        	
        	Paused = false; 
        	 	
        }
          
    	if (Playing == true && Paused == false) {
    			
            		switch (e.getAction()) {

            		case MotionEvent.ACTION_MOVE:
            			playerX = x;
            			playerY = y;
            			break;
            			
            		case MotionEvent.ACTION_UP:
            			Paused = true;
            		}
        		}

        return true;
    }
    
}
