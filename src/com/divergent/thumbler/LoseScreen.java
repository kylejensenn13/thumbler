package com.divergent.thumbler;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

public class LoseScreen extends View {

	public RectF testRect;
	public RectF menuBounds;
	public Paint paint;
	public boolean test = false;
	public Paint finalScorePaint;
	public String finalScoreString;
	public Integer HighScore;
	public AssetManager Assets;
	public Typeface font;
	
	public Bitmap loseMenu;
	public int screenHeight, screenWidth;
	
	
	public LoseScreen(Context context) {
		super(context);
		
		testRect = new RectF();
		menuBounds = new RectF();
		
		paint = new Paint();
		finalScorePaint = new Paint();
		
		
		Assets = getContext().getAssets();
		HighScore = SharedPrefManager.getHighScore();
		font = Typeface.createFromAsset(Assets, "fonts/EuphoriaScript.otf");
		
        // Get window size and save it to screenWidth and screenHeight.
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size); 
        screenWidth = size.x;
        screenHeight = size.y;

	}
	
	@Override protected void onDraw(Canvas canvas) {
		
		testRect.set(0, 0, 100, 100);
		paint.setColor(Color.GREEN);
		canvas.drawRect(testRect, paint);
				
		finalScorePaint.setTextSize(72);
		finalScorePaint.setTypeface(font);
		finalScorePaint.setColor(Color.WHITE);
		
		finalScoreString = PlayScreen.finalScore.toString();
		canvas.drawText("Your Score is:", 250, 450, finalScorePaint);
		canvas.drawText(finalScoreString, 300, 500, finalScorePaint);
		canvas.drawText(HighScore.toString(), 400, 550, finalScorePaint);

		
		ViewGroup parent = (ViewGroup)getParent();
		
		if (test == true) {	
			parent.addView(new PlayScreen(getContext()));
			parent.removeView(this);
			parent.setBackgroundResource(R.drawable.background);
		}
		
        // Delay	
        try { Thread.sleep(10); }
        catch (InterruptedException e){}
        invalidate(); // Force a re-draw
	}
	
    public boolean onTouchEvent(MotionEvent e) {
    	
    	// Get finger's X and Y position
        float x = e.getX();
        float y = e.getY();
        
        
		
        if (x > testRect.left && x < testRect.right && y < testRect.bottom && y > testRect.top) {
        	test = true;
        }
    
        return true;
    }

}
