package com.divergent.thumbler;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.RectF;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

public class MainMenu extends View {
	
	private RectF titleBounds, playBounds, scoreBounds, soundBounds, creditBounds;
	private Paint titlePaint, playPaint;
	
	private boolean playClick = false, startPlay = false, scoreClick = false, startScore = false, soundClick = false, startSound = false, creditsClick = false, startCredits = false;
	
	
	public Bitmap play;
	public Bitmap playGlow;

	public Bitmap sound;
	public Bitmap soundGlow;

	
	public int screenWidth, screenHeight;

	public MainMenu(Context context) {
		super(context);
		
		titleBounds = new RectF();
		playBounds = new RectF();
		scoreBounds = new RectF();
		soundBounds = new RectF();
		creditBounds = new RectF();
		
		titlePaint = new Paint();
		playPaint = new Paint();
		
        play = BitmapFactory.decodeResource(getResources(), R.drawable.play);
        playGlow = BitmapFactory.decodeResource(getResources(), R.drawable.playglow);

        sound = BitmapFactory.decodeResource(getResources(), R.drawable.sound);
        soundGlow = BitmapFactory.decodeResource(getResources(), R.drawable.soundglow);
        
        
        // Get window size and save it to screenWidth and screenHeight.
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size); 
        screenWidth = size.x;
        screenHeight = size.y;
	}
	
	
	@Override protected void onDraw(Canvas canvas) {
		
    	
		ViewGroup parent = (ViewGroup)getParent();
		
		playBounds.set(screenWidth / 2 - play.getWidth() / 2, screenHeight * 1/3 - play.getHeight() / 2, playBounds.left + play.getWidth(), playBounds.top + play.getHeight());
		soundBounds.set(scoreBounds.centerX() - sound.getWidth() / 2, scoreBounds.bottom + 10, soundBounds.left + sound.getWidth(), soundBounds.top + sound.getHeight());
		
		
		if (playClick == false) canvas.drawBitmap(play, null, playBounds, null);
		else canvas.drawBitmap(playGlow, null, playBounds, null);	
				
		if (soundClick == false) canvas.drawBitmap(sound, null, soundBounds, null);
		else canvas.drawBitmap(soundGlow, null, soundBounds, null);	
		
		
		if (startPlay == true) {
			parent.addView(new PlayScreen(getContext()));
			parent.removeView(this);
			parent.setBackgroundResource(R.drawable.background);
		} else if (startSound == true) {
				
		}
        // Delay	
        try { Thread.sleep(10); }
        catch (InterruptedException e){}
        invalidate(); // Force a re-draw
	}
	
    public boolean onTouchEvent(MotionEvent e) {
    

    	// Get finger's X and Y position
        float x = e.getX();
        float y = e.getY();

        	switch (e.getAction()) {
        	
        	case MotionEvent.ACTION_DOWN:
        		
                if(x > playBounds.left && x < playBounds.right && y < playBounds.bottom && y > playBounds.top) {
                	playClick = true;
                } else if(x > soundBounds.left && x < soundBounds.right && y < soundBounds.bottom && y > soundBounds.top) {
                	soundClick = true;
                }
                
            break;
        	case MotionEvent.ACTION_UP:
        		
        		if (playClick == true) startPlay = true;	       			
        		else if (soundClick == true) startSound = true;
        		
        		break;
        	
        	}

        return true;
    }

}



