package com.divergent.thumbler;

import com.divergent.thumbler.SharedPrefManager;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends Activity {
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
       super.onCreate(savedInstanceState);
       View PlayScreen = new MainMenu(this);
       setContentView(PlayScreen);
       PlayScreen.setBackgroundResource(R.drawable.background);

       SharedPrefManager.Init(this);

    }
}
